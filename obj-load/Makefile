
APPNAME:=$(shell basename `pwd`)

# for inst target instrumentation very powerful for debugging
INSTR:= -fsanitize=address,leak,undefined,pointer-compare,pointer-subtract
INSTR+= -fno-omit-frame-pointer -g 

# bgfx debug and release libs
BGFXLIBSD:= -lbgfxDebug -lbimg_decodeDebug  -lbimgDebug -lbxDebug
BGFXLIBSR:= -lbgfxRelease  -lbimg_decodeRelease -lbimgRelease -lbxRelease

# libs only actually need GL for OpenGL render target (see define in main.c)
LDFLAGS:=  -L../bgfx/.build/linux64_gcc/bin
LDFLAGS+= -lglfw -lGL -lX11 -ldl -lpthread -lrt -lm -lstdc++

# decent warnings save time in the long run
CFLAGS:= -std=c99 -Wfatal-errors -Wall -Wextra -Werror
INCS:= -I include -I ../bgfx/include -I ../bx/include -I ../bgfx/3rdparty/ -I ../bimg/include

# discover the source code and create the .o targets
SRC:=$(wildcard src/*.c)
OBJ:=$(SRC:src/%.c=.build/%.o)

# shader binary targets for Vulkan and OpenGL
SDRS:= data/spirv/vs_tex.bin data/spirv/fs_tex.bin data/glsl/vs_tex.bin data/glsl/fs_tex.bin

CC=gcc

# shader compiler
SDRC = ../bgfx/.build/linux64_gcc/bin/shadercDebug

all: debug

$(APPNAME): $(OBJ) $(SDRS)
	$(CC) $(OBJ) -o $(APPNAME) $(LDFLAGS)

$(OBJ): .build/%.o : src/%.c
	$(CC) $(CFLAGS) $(INCS) -c $< -o $@

# shader targets could probably do something "clever" here and
# have one or two general purpose rules, but life is too short...
data/spirv/vs_tex.bin: src/vs_tex.sc src/varying.def_tex.sc
	$(SDRC) -f $< --varyingdef src/varying.def_tex.sc -i . -o $@ --platform linux --type vertex -p spirv

data/glsl/vs_tex.bin: src/vs_tex.sc src/varying.def_tex.sc
	$(SDRC) -f $< --varyingdef src/varying.def_tex.sc -i . -o $@ --platform linux --type vertex -p 150
	
data/spirv/fs_tex.bin: src/fs_tex.sc src/varying.def_tex.sc
	$(SDRC) -f $< --varyingdef src/varying.def_tex.sc -i . -o $@ --platform linux --type fragment -p spirv
	
data/glsl/fs_tex.bin: src/fs_tex.sc src/varying.def_tex.sc
	$(SDRC) -f $< --varyingdef src/varying.def_tex.sc -i . -o $@ --platform linux --type fragment -p 150

.PHONY: debug release inst

# build with debug info
debug: CFLAGS+= -g
debug: LDFLAGS+= $(BGFXLIBSD)
debug:
	@echo "*** made DEBUG target ***"

# build without debug info and optimise
release: CFLAGS+= -Ofast
release: LDFLAGS+= $(BGFXLIBSR)
release:
	@echo "*** made RELEASE target ***"

# libasan rule for memory debugging
inst: CFLAGS+= $(INSTR)
inst: LDFLAGS+= $(INSTR)  $(BGFXLIBSD)
inst: 
	@echo "*** made INSTRUMENTATION target ***"

debug release inst: $(APPNAME)

.PHONY:	clean
clean:
	rm .build/* -f
	rm $(APPNAME) -f
	rm $(SDRS) -f

