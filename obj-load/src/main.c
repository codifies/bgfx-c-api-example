/*
 * Copyright (c) 2021 Chris Camacho (Codifies -  http://bedroomcoders.co.uk/)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
#include <stdio.h>
#include <string.h>
#include <limits.h>

#include <bgfx/c99/bgfx.h>

#include "GLFW/glfw3.h"


// todo have seperate source units per renderer
// so renderer could be selected at run time.

// uncomment to use GL instead of Vulkan
//#define RENDERERGL


#if BX_PLATFORM_LINUX || BX_PLATFORM_BSD 
    #ifdef RENDERERGL
        #define GLFW_EXPOSE_NATIVE_GLX // for opengl renderer
    #else
        #define GLFW_EXPOSE_NATIVE_X11
    #endif
#else
// TODO gah loath to boot windows... need a few windows renderers 
// fallback and something next gen, contribution appreciated
    #define GLFW_EXPOSE_NATIVE_WIN32
#endif

#include "GLFW/glfw3native.h"

#define MATH_3D_IMPLEMENTATION
#include "math_3d.h"

#include "model.h"

// TODO make these variables changed by glfx window resize callback
#define WNDW_WIDTH 960
#define WNDW_HEIGHT 540

#define MAX_LIGHTS 4



int main(void)
{
    //------------------------------------------------------------------
    // create a window that bgfx can use
    //------------------------------------------------------------------
    
    glfwInit();
    #ifdef RENDERERGL
        glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API); // for opengl renderer
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    #else
        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API); // for Vulcan renderer type
    #endif

    GLFWwindow* window = glfwCreateWindow(WNDW_WIDTH, WNDW_HEIGHT, "Hello, bgfx!", NULL, NULL);

    // set up info about the platform for BGFX
    bgfx_platform_data_t pd;
    #if BX_PLATFORM_LINUX || BX_PLATFORM_BSD 
        #if ENTRY_CONFIG_USE_WAYLAND // examples entry options define
            pd.ndt     = glfwGetWaylandDisplay(); 
        #else 
            #ifdef RENDERERGL
                pd.ndt = NULL; // for opengl renderer
                pd.nwh = (void*)glfwGetGLXWindow(window);
                pd.context = glfwGetGLXContext(window);            
            #else
                pd.ndt = glfwGetX11Display(); // for Vulcan
                pd.nwh = (void*)glfwGetX11Window(window);            
            #endif
        #endif 
    #elif BX_PLATFORM_OSX
        pd.ndt      = NULL; 
    #elif BX_PLATFORM_WINDOWS 
        pd.ndt      = NULL; 
        pd.nwh      = glfwGetWin32Window(window);
    #endif // BX_PLATFORM_*

    bgfx_init_t bgfxInit;
    bgfx_init_ctor(&bgfxInit);
    
    #ifdef RENDERERGL
        bgfxInit.type = BGFX_RENDERER_TYPE_OPENGL;
    #else
        bgfxInit.type = BGFX_RENDERER_TYPE_VULKAN;   
    #endif
    
    bgfxInit.resolution.width = WNDW_WIDTH;
    bgfxInit.resolution.height = WNDW_HEIGHT;
    bgfxInit.resolution.reset = BGFX_RESET_VSYNC;    
    // seems bgfx is bright enough to not use the active but unused integrated device! (card0!)
    //bgfxInit.vendorId = BGFX_PCI_ID_NVIDIA;
    bgfxInit.platformData = pd;
    bgfx_init(&bgfxInit);
    
    //------------------------------------------------------------------
    // BGFX is now ready for use!
    //------------------------------------------------------------------
    
    printf("using %s renderer\n",bgfx_get_renderer_name(bgfx_get_renderer_type()));

    bgfx_set_view_clear(0, BGFX_CLEAR_COLOR | BGFX_CLEAR_DEPTH, 0x443355FF, 1.0f, 0);
    bgfx_set_view_rect(0, 0, 0, WNDW_WIDTH, WNDW_HEIGHT);
    
    //------------------------------------------------------------------
    // shader creation
    //
    // TODO some sort of stuct to hold shaders and their 
    // uniforms to bundle the "standard" simple light variables
    //------------------------------------------------------------------
    
    bgfx_shader_handle_t vsh = loadShader("vs_tex.bin");
    printf("shader handle %i created for vs_tex.bin\n", vsh.idx);
    if (vsh.idx == USHRT_MAX)
    {
        printf("*** shader model not supported or file not found ****\n");
        bgfx_shutdown();
        return -1;
    }

    bgfx_shader_handle_t fsh = loadShader("fs_tex.bin");
    printf("shader handle %i created for fs_tex.bin \n", fsh.idx);
    if (fsh.idx == USHRT_MAX)
    {
        printf("*** shader model not supported or file not found ****\n");
        bgfx_shutdown();
        return -1;
    }

    bgfx_program_handle_t program = bgfx_create_program(vsh, fsh, true);
    printf("program handle %i created\n", program.idx);
    
    // each material from the model has a diffuse texture AND colour
    // the texture is optional (a default 1x1 white dot is used)
    bgfx_uniform_handle_t difftxU = bgfx_create_uniform("diffuseTX", BGFX_UNIFORM_TYPE_SAMPLER, 1);
    bgfx_uniform_handle_t diffcU = bgfx_create_uniform("diffuseCol", BGFX_UNIFORM_TYPE_VEC4, 1);
    
    // simple lighting
    //bgfx_uniform_handle_t vPosU = bgfx_create_uniform("viewPos", BGFX_UNIFORM_TYPE_VEC4, 1);
    bgfx_uniform_handle_t lPosU = bgfx_create_uniform("lightPos", BGFX_UNIFORM_TYPE_VEC4, MAX_LIGHTS);
    bgfx_uniform_handle_t lColU = bgfx_create_uniform("lightColour", BGFX_UNIFORM_TYPE_VEC4, MAX_LIGHTS);
    
    
    //------------------------------------------------------------------
    // model loading
    //------------------------------------------------------------------
    
    initModels();    // setup model loader

    // dice simple cube with a texture
    Model dice = LoadObj("data/dice.obj");   
     //apply shader to each material, could have different shaders for some
     //or all materials...
    applyObjShader(&dice, program, diffcU, difftxU);
   
    // ambulance untextured, but multiple materials
    // with different diffuse colour
    Model amb = LoadObj("data/ambulance.obj");
    applyObjShader(&amb, program, diffcU, difftxU);
    
    // munged together shapes to help test lighting!
    // three textures
    Model thing = LoadObj("data/thing.obj");
    applyObjShader(&thing, program, diffcU, difftxU);
    
    // ground plane single texture
    Model plane = LoadObj("data/plane.obj");
    applyObjShader(&plane, program, diffcU, difftxU);
    
    // marker for lights simple cube no texture
    Model mark = LoadObj("data/marker.obj");
    applyObjShader(&mark, program, diffcU, difftxU);

    // light positions
    float lpos[] = {
        -8, 6, -8, 0,
         8, 6, -8, 0,
        -8, 6,  8, 0,
         8, 6,  8, 0
    };
    
    // light colours
    float lcol[] = {
        1, 0, 0, 1,
        0, 1, 0, 1,
        0, 0, 1, 1,
        1, 1, 1, 1
    };
       
    // light colours could change every frame, but here
    // they are always the same red, green, blue and white
    // so set them up once and leave them
    bgfx_set_uniform(lColU, lcol, MAX_LIGHTS);
    
    float lightAng = 0;
    float camAng = 0;
    float modAng = 0;
    
    while(!glfwWindowShouldClose(window)) 
    {
        if ( glfwGetKey(window, GLFW_KEY_LEFT) ) camAng -= 0.01;
        if ( glfwGetKey(window, GLFW_KEY_RIGHT) ) camAng += 0.01;

        lightAng += 0.005;
        modAng += 0.011;

        //--------------------------------------------------------------
        // set up the view and projection
        //--------------------------------------------------------------
        
        // Set view 0 default viewport.
        bgfx_set_view_rect(0, 0, 0, (uint16_t)WNDW_WIDTH, (uint16_t)WNDW_HEIGHT);
   
        vec3_t at = vec3(0.0f, 1.0f,  0.0f);
        vec3_t eye = vec3(sin(camAng) * 12, 8, cos(camAng)*12 );
        vec3_t up = vec3(0.0f, 1.0f, 0.0f);
        mat4_t view = m4_look_at( eye, at, up );
        mat4_t proj = m4_perspective(60.0f, (float)(WNDW_WIDTH) / (float)(WNDW_HEIGHT),  0.01f, 1000.f);

        bgfx_set_view_transform(0, &view, &proj);
        // The "camera" is now set...

        //--------------------------------------------------------------
        // model rendering
        //--------------------------------------------------------------
                
        // move the lights around, place a marker of the correct 
        // colour at each lights position
        for (int i = 0; i < 4; i++) 
        {
            // red moves slowest, white fastest
            lpos[i*4+0] = sin(lightAng*(i+1.567))*16;
            lpos[i*4+2] = cos(lightAng*(i+1.567))*16;
            
            mat4_t mtx = m4_translation(vec3(lpos[i*4+0],lpos[i*4+1],lpos[i*4+2]));
            bgfx_set_transform(&mtx, 1);
            
            // change the material diffuse colour to match light colour
            mark.materials[0].diffuse.r = lcol[i*4+0];
            mark.materials[0].diffuse.g = lcol[i*4+1];
            mark.materials[0].diffuse.b = lcol[i*4+2];
            
            submitModel(&mark);
        }

        // send the light info to the shader
        //bgfx_set_uniform(vPosU, &eye, 1);
        bgfx_set_uniform(lPosU, lpos, MAX_LIGHTS);

        
        // matricies to do something with the loaded models
        mat4_t mtx,mtx_x,mtx_y;

        // rotate and translate the dice model
        vec3_t rv = vec3(modAng, -modAng *.77, 0 );
        mtx_y = m4_rotation_y(rv.y);
        mtx_x = m4_rotation_x(rv.x);
        mtx = m4_mul(mtx_y, mtx_x);
        mtx = m4_set_translation(mtx, vec3(0,2,0));
        bgfx_set_transform(&mtx, 1);

        submitModel(&dice);
        
        // use combined rotation with translation
        mtx = m4_rotation_zyx(rv);
        mtx = m4_set_translation(mtx, vec3(-3,2,0));
        bgfx_set_transform(&mtx, 1);

        submitModel(&amb);
        
        // draw the test shape
        mtx = m4_rotation_zyx(rv);
        mtx = m4_set_translation(mtx, vec3(4,2,0));
        bgfx_set_transform(&mtx, 1);

        submitModel(&thing);
        
        // draw the ground plane
        mtx = m4_identity();
        bgfx_set_transform(&mtx, 1);
        
        submitModel(&plane);
        

        //--------------------------------------------------------------
        // render all the submitted models
        //--------------------------------------------------------------
        bgfx_frame(false);
        
        //--------------------------------------------------------------
        // check input an exit on escape - poll events also allows
        // glfwGetKey above to work
        //--------------------------------------------------------------
        glfwPollEvents();
        if ( glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        {
            glfwSetWindowShouldClose(window, true);
        }
    }

    freeModel(&dice);
    freeModel(&amb);
    freeModel(&thing);
    freeModel(&plane);
    freeModel(&mark);

    shutdownModels();
        
    bgfx_destroy_uniform(difftxU);
    bgfx_destroy_uniform(diffcU);
    //bgfx_destroy_uniform(vPosU);
    bgfx_destroy_uniform(lPosU);
    bgfx_destroy_uniform(lColU);
    
    bgfx_destroy_program(program);
    
    bgfx_shutdown();       
    
    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;

}
