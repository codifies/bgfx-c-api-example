/*
 * Copyright (c) 2020 Chris Camacho (Codifies -  http://bedroomcoders.co.uk/)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "util.h"

//STBI_MALLOC, STBI_REALLOC, and STBI_FREE



#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

// file io stuff "borrowed" from raylib 

#if !defined(__linux__)
    #include <direct.h>     // Required for: _chdir() [Used in LoadObj()]
    #define CHDIR _chdir
#else
    #include <unistd.h>     // Required for: chdir() (POSIX) [Used in LoadObj()]
    #define CHDIR chdir
#endif

// TODO use bgfx versions ?
// TODO find replace on RL_ macros
//#define RL_FREE free
//#define RL_CALLOC calloc
//#define RL_MALLOC malloc
//#define RL_REALLOC realloc

// uses stb to load up image data also setting width/height and bpp
ImgData loadImage(const char* fileName) 
{
    ImgData img;
	img.data = stbi_load( fileName, &img.width, &img.height, &img.bpp, 4 );
	// img.data is now 4 bytes per pixel, width*height*size(4). Or NULL if load failed.
	return img;
}

void freeImageData(ImgData img)
{
    stbi_image_free( img.data );
}


// String pointer reverse break: returns right-most occurrence of charset in s
static const char *strprbrk(const char *s, const char *charset)
{
    const char *latestMatch = NULL;
    for (; s = strpbrk(s, charset), s != NULL; latestMatch = s++) { }
    return latestMatch;
}

// Get directory for a given filePath
const char *GetDirectoryPath(const char *filePath)
{

    // NOTE: Directory separator is different in Windows and other platforms,
    // fortunately, Windows also support the '/' separator, that's the one should be used
    // TODO if I ever bother with windows...
    //#if defined(_WIN32)
        //char separator = '\\';
    //#else
        //char separator = '/';
    //#endif

    const char *lastSlash = NULL;
    static char dirPath[MAX_FILEPATH_LENGTH];
    memset(dirPath, 0, MAX_FILEPATH_LENGTH);

    // In case provided path does not contain a root drive letter (C:\, D:\) nor leading path separator (\, /),
    // we add the current directory path to dirPath
    if (filePath[1] != ':' && filePath[0] != '\\' && filePath[0] != '/')
    {
        // For security, we set starting path to current directory,
        // obtained path will be concated to this
        dirPath[0] = '.';
        dirPath[1] = '/';
    }

    lastSlash = strprbrk(filePath, "\\/");
    if (lastSlash)
    {
        if (lastSlash == filePath)
        {
            // The last and only slash is the leading one: path is in a root directory
            dirPath[0] = filePath[0];
            dirPath[1] = '\0';
        }
        else
        {
            // NOTE: Be careful, strncpy() is not safe, it does not care about '\0'
            memcpy(dirPath + (filePath[1] != ':' && filePath[0] != '\\' && filePath[0] != '/' ? 2 : 0), filePath, strlen(filePath) - (strlen(lastSlash) - 1));
            dirPath[strlen(filePath) - strlen(lastSlash) + (filePath[1] != ':' && filePath[0] != '\\' && filePath[0] != '/' ? 2 : 0)] = '\0';  // Add '\0' manually
        }
    }

    return dirPath;
}

// Get current working directory (originally from raylib!)
const char *GetWorkingDirectory(void)
{
    static char currentDir[MAX_FILEPATH_LENGTH];
    memset(currentDir, 0, MAX_FILEPATH_LENGTH);

    char *ptr = GETCWD(currentDir, MAX_FILEPATH_LENGTH - 1);

    return ptr;
}

// borrowed from Raylib and butchered by codifies
char *LoadFileText(const char *fileName)
{
    char *text = NULL;

    if (fileName != NULL)
    {
        FILE *file = fopen(fileName, "rt");

        if (file != NULL)
        {
            // WARNING: When reading a file as 'text' file,
            // text mode causes carriage return-linefeed translation...
            // ...but using fseek() should return correct byte-offset
            fseek(file, 0, SEEK_END);
            unsigned int size = (unsigned int)ftell(file);
            fseek(file, 0, SEEK_SET);

            if (size > 0)
            {
                text = malloc((size + 1)*sizeof(char));
                unsigned int count = (unsigned int)fread(text, sizeof(char), size, file);

                // WARNING: \r\n is converted to \n on reading, so,
                // read bytes count gets reduced by the number of lines
                if (count < size) text = realloc(text, count + 1);

                // Zero-terminate the string
                text[count] = '\0';

                printf("FILEIO: [%s] Text file loaded successfully\n", fileName);
            }
            else printf("FILEIO: [%s] Failed to read text file\n", fileName);

            fclose(file);
        }
        else printf("FILEIO: [%s] Failed to open text file\n", fileName);

    }
    else printf("FILEIO: File name provided is not valid\n");

    return text;
}

 
