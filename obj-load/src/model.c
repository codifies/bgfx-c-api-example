/***********************************************************************
 * 2020/11                                                             *
 *                                                                     *
 * Wavefront OBJ model loader implemented from scratch by Codifies     *
 * seperates mesh into a mesh per material                             *
 *                                                                     *
 *                                                                     *
 * TODO's                                                              *
 *                                                                     *
 * only diffuse map and colour used                                    *
 *                                                                     *
 ***********************************************************************/

/*
 * Copyright (c) 2020 Chris Camacho (Codifies -  http://bedroomcoders.co.uk/)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */ 

#include <string.h>

// at some point OBJ specific stuff should be seperated out
// of models, at the moment only OBJ loading is supported
#include "math_3d.h"
#include <bgfx/c99/bgfx.h>

#include "util.h"
#include "model.h"

// all Models should specify position, normal and UV's for each
// vertex
// even if a vertex has the same position if the normal or
// UV data is different then it is a unique vertex
static bgfx_vertex_layout_t objvLayout;
static bgfx_vertex_layout_handle_t layoutH;

// default texture, often used where there is only a diffuse colour
static const unsigned char defaultTxData[] = 
{
    0xff, 0xff, 0xff, 0xff // 1x1 WHITE dot
};

static bgfx_texture_handle_t defaultTx;

// call this before any other model function
void initModels()
{
    bgfx_vertex_layout_begin(&objvLayout, bgfx_get_renderer_type());
    bgfx_vertex_layout_add(&objvLayout, BGFX_ATTRIB_POSITION, 3, BGFX_ATTRIB_TYPE_FLOAT, false, false);
    bgfx_vertex_layout_add(&objvLayout, BGFX_ATTRIB_NORMAL, 3, BGFX_ATTRIB_TYPE_FLOAT, false, false);
    bgfx_vertex_layout_add(&objvLayout, BGFX_ATTRIB_TEXCOORD0, 2, BGFX_ATTRIB_TYPE_FLOAT, false, false);
    bgfx_vertex_layout_end(&objvLayout);
    
    layoutH = bgfx_create_vertex_layout(&objvLayout);
    
    // default texture
    const bgfx_memory_t* txMem = bgfx_make_ref(defaultTxData, 4); 
    defaultTx = bgfx_create_texture_2d(1, 1, false, 0, BGFX_TEXTURE_FORMAT_RGBA8, 0 , txMem);
}

// call this only after there is no need to render models (on exit typically)
void shutdownModels()
{
    bgfx_destroy_vertex_layout(layoutH);
    // NB nothing to "free" with default texture
}

bgfx_shader_handle_t loadShader(const char *FILENAME)
{
    const char* shaderPath = "";

    //dx11/  dx9/   essl/  glsl/  metal/ pssl/  spirv/
    bgfx_shader_handle_t invalid = BGFX_INVALID_HANDLE;
    
    // currently only glsl and spirv binaries supported

    switch(bgfx_get_renderer_type()) {
        case BGFX_RENDERER_TYPE_NOOP:
        case BGFX_RENDERER_TYPE_DIRECT3D9:     shaderPath = "dx9/";   break;
        case BGFX_RENDERER_TYPE_DIRECT3D11:
        case BGFX_RENDERER_TYPE_DIRECT3D12:    shaderPath = "dx11/";  break;
        case BGFX_RENDERER_TYPE_GNM:           shaderPath = "pssl/";  break;
        case BGFX_RENDERER_TYPE_METAL:         shaderPath = "metal/"; break;
        case BGFX_RENDERER_TYPE_OPENGL:        shaderPath = "data/glsl/";  break; //*
        case BGFX_RENDERER_TYPE_OPENGLES:      shaderPath = "essl/";  break;
        case BGFX_RENDERER_TYPE_VULKAN:        shaderPath = "data/spirv/"; break; //*
        case BGFX_RENDERER_TYPE_NVN:
        case BGFX_RENDERER_TYPE_WEBGPU:
        case BGFX_RENDERER_TYPE_COUNT:         return invalid; // count included to keep compiler warnings happy
    }

    size_t shaderLen = strlen(shaderPath);
    size_t fileLen = strlen(FILENAME);
    char *filePath = (char *)malloc(shaderLen + fileLen + 1);
    memcpy(filePath, shaderPath, shaderLen);
    memcpy(&filePath[shaderLen], FILENAME, fileLen);
    filePath[shaderLen+fileLen] = 0;    // properly null terminate

    printf("filepath = %s\n", filePath);
    FILE *file = fopen(filePath, "rb");
    
    if (!file) {
        return invalid;
    }
    
    fseek(file, 0, SEEK_END);
    long fileSize = ftell(file);
    fseek(file, 0, SEEK_SET);

    const bgfx_memory_t *mem = bgfx_alloc(fileSize + 1);
    fread(mem->data, 1, fileSize, file);
    mem->data[mem->size - 1] = '\0';
    fclose(file);

    free(filePath);
    return bgfx_create_shader(mem);
}


bgfx_texture_handle_t loadTextureForBgfx( const char* fileName)
{
    ImgData Img = loadImage(fileName);
    printf("fileName %s %p\n",fileName, (void*)Img.data);
    // TODO warn and use default texture if file can't be loaded
    // rather than seg-fault !
    const bgfx_memory_t* txMem = bgfx_copy(Img.data, Img.width*Img.height*4); 
    
    bgfx_texture_handle_t txh = bgfx_create_texture_2d(Img.width, Img.height, false, 0, BGFX_TEXTURE_FORMAT_RGBA8, 0 , txMem);
    bgfx_set_texture_name(txh, fileName, strlen(fileName));
    freeImageData(Img);
    return txh;
}

void submitMesh( Mesh* m, Material mat )
{ 

    bgfx_set_vertex_buffer(0, m->vbh, 0, m->vertexCount);
    bgfx_set_uniform(mat.diffcU, &mat.diffuse, 1);
    bgfx_set_texture(0, mat.difftxU, mat.difftx, 0); // TODO allow user defined texture flags ?

    bgfx_submit(0, mat.shader, 0, BGFX_DISCARD_BINDINGS | BGFX_DISCARD_STATE);

}

void submitModel( Model* m )
{
    for (int i = 0; i < m->meshCount; i++)
    {
        submitMesh( &m->meshes[i], m->materials[m->meshMaterial[i]] );
    }
}


// in future there will me more stuff to free...
// just a bunch of handles the stack can cope!
void freeMaterial(Material m)
{
    // intelligently handles multiple calls with the default texture
    bgfx_destroy_texture(m.difftx);
}

// could be large, on some platforms stack might not be big enough...
void freeMesh(Mesh* m) 
{
    free(m->buffer);
    bgfx_destroy_vertex_buffer(m->vbh);
}

void freeModel(Model* m)
{
    for (int i = 0; i< m->materialCount; i++)
    {
        freeMaterial(m->materials[i]);
        
    }
    for (int i=0; i < m->meshCount; i++) {
        freeMesh(&m->meshes[i]);
    }
    free(m->meshes);
    free(m->materials);
    free(m->meshMaterial);

}

// applies a single shader to all the materials
// (you could have a different shader for each material if
// you wanted...)
// params_split_for_long_type_names !
void applyObjShader(Model* mod, 
                        bgfx_program_handle_t program, 
                        bgfx_uniform_handle_t diffcU, 
                        bgfx_uniform_handle_t difftxU)
{
    for ( int i = 0; i < mod->materialCount; i++ )
    {
        mod->materials[i].shader = program;
        mod->materials[i].diffcU = diffcU;
        mod->materials[i].difftxU = difftxU;
    }
}

// structure to hold face indexes
// initially before triangulation a face can have > 3 verts
typedef struct faceInfo {
    int *vi, *ti, *ni;
    int numVerts;
    struct faceInfo* next; // a face can only be in a single list....
} faceInfo;

// a list of faces
typedef struct faceList {
    faceInfo* first;
    faceInfo* last;
    int faceCount;
} faceList;

// TODO make this set by material count
#define MAXFACEMATS 128

// a list of all faces by material
faceList allFaces[MAXFACEMATS] = { 0 };

// copy string till the end of line in source string
static inline void strCpyEOL(char* src, char* dst) 
{
    int i=0;
    char tmp[1024];
    while (src[0]!='\n' && src[0]!='\r' && src[0]!=0) {
        tmp[i++] = src[0];
        src++;
    }
    tmp[i]=0;
    strncpy(dst, tmp, 1024);
}

// scan forward for the start of the next line or leave
// on NULL char at end...
static inline char* nextLine(char* line) 
{
    while (*line != '\n') { line++; } // find end of line
    while (*line != 0 && *line < 32) { line++; } // find start of next
    return line;  
}

// maintain the face count and chain of next ptrs
static void addFace(faceInfo* f, int matID) 
{
    if (allFaces[matID].first==NULL) {  allFaces[matID].first = f;  }
    
    if (allFaces[matID].last!=NULL) { 
        allFaces[matID].last->next = f;
    }
        
    allFaces[matID].last = f;
    f->next = NULL;
    allFaces[matID].faceCount++;
}

// release all the memory allocated to the faces
static void releaseFaces()
{
    for (int i=0; i<MAXFACEMATS; i++) {
        faceInfo* node = allFaces[i].first;
        while (node) {
            faceInfo* nextNode = node->next;
            free(node->vi);
            if (node->ti) free(node->ti);
            if (node->ni) free(node->ni);
            free(node);
            node = nextNode;
        }
    }
}

// count how many times a character is in a string 
static inline int countChars(char* in, char c) 
{
    int i=0, n=0;
    while (in[i] != '\0') {
        if (in[i] == c) {  n++;  }
        i++;
    }
    return n;
}

/* a triangle face could be formatted like.... 
 * case 1   v/t/n
 * case 2   v//n 
 * case 3   v/t
 * case 4   v
 */
static void parseVert(char* in, int corner, faceInfo* ti) 
{
    int ns = countChars(in, '/');
    
    if (ns == 2) {                          // case 1 or 2
        if (!strstr(in,"//")) {             // case 1
            sscanf( in, "%i/%i/%i", &ti->vi[corner], &ti->ti[corner], &ti->ni[corner] );
        } else {                            // case 2
            sscanf( in, "%i//%i", &ti->vi[corner], &ti->ni[corner] );
        }
    }
    
    if (ns == 1) {                          // case 3
        sscanf( in, "%i/%i", &ti->vi[corner], &ti->ti[corner]);
    }
    
    if (ns == 0) {                          // case 4
        sscanf( in, "%i", &ti->vi[corner]);
    }  
}

// copy from one string to another till you hit the end of the string
// or a specific character in the source string
// returns the location in the source string 1 character after
// the token is found, so one string can be split into multiple.
static char* cpyStrTill(char* src, char* dst, char till) 
{
    int i=0;
    while (src[i] != till && src[i] != '\r' && src[i] != '\n' && src[i] != 0) {
        dst[i] = src[i];
        i++;
    }
    dst[i] = '\0';
    i++;
    return &src[i];
} 

static faceInfo* parseFace(char* in) 
{
    // first split into 3 strings
    char s1[80],s2[80],s3[80];
    
    char* s = in;
    s = cpyStrTill( s, s1, ' ');
    s = cpyStrTill( s, s2, ' ');
    s = cpyStrTill( s, s3, ' ');
    
    char* ss = s; // save the point past first tri
    int ex = 0; // count the extra verts
    while(s[0]) {
        char l[80];
        char* ls = s;
        s = cpyStrTill(s, l, ' ');
        if (s!=ls+1) {  ex++;  }
    }
    
    // allocate space for triangle / polygon verts 
    faceInfo* face = calloc(1, sizeof(faceInfo)); 
    face->numVerts = ex+3;
    face->vi = calloc(ex+3, sizeof(int));
    face->ti = calloc(ex+3, sizeof(int));
    face->ni = calloc(ex+3, sizeof(int));
    
    // then parse each sub string pf 1st tri
    parseVert(s1, 0, face);
    parseVert(s2, 1, face);
    parseVert(s3, 2, face);
    
    s=ss; // if its a polygon add in the extra verts
    int c = 3;
    while(s[0]) {
        char l[80];
        char* ls = s;
        s = cpyStrTill(s, l, ' ');
        if (s!=ls+1) {  parseVert(l, c, face); c++; }
    }

    return face;
}



Model LoadObj(const char* filename)
{
    char *line;
    Model model = { 0 };
    char currentDir[1024] = { 0 };
    char tmpStr[1024] = { 0 };
    
    // clear all the face lists
    for (int i=0; i< MAXFACEMATS; i++) {
        allFaces[i].faceCount = 0;
        allFaces[i].first = NULL;
        allFaces[i].last = NULL;
    }
    
    // save current dir to restore later
    strcpy(currentDir, GetWorkingDirectory());

    // load in the obj text file
    char* objdata = LoadFileText(filename);
    char* mtldata = NULL;
    CHDIR(GetDirectoryPath(filename));
    
    if (!objdata) {
        return model;
    }
    
    // scan through obj find the material file and also
    // total the vert, normals, texture coords and faces
    int vc = 0;        // counts for verts, norms, tx coords and faces
    int nc = 0;
    int tc = 0;
    int faces = 0;
    line = objdata;
    
    while(line[0]!='\0') 
    {
        // spec says there should only be one mtllib
        if (strncmp("mtllib ", line, 7)==0) {
            strCpyEOL(line+7, tmpStr);
            mtldata = LoadFileText(tmpStr);
        }
        // counts
        if (strncmp("v ", line, 2)==0) {  vc++;  }
        if (strncmp("vn ", line, 3)==0) {  nc++;  }
        if (strncmp("vt ", line, 3)==0) {  tc++;  }
        if (strncmp("f ", line, 2)==0) {  faces++;  }

        line = nextLine(line);
    }

    // count the materials
    int materialCount=0;
    line = mtldata;
    if (line) {  // there need not be a mtl file
        while (line[0] != '\0') {
            if (strncmp("newmtl ", line, 7)==0) {
                materialCount++;
            }
            line = nextLine(line); 
        }
    }
    
    // at least one mesh/material which if no mtl will be default WHITE
    if (materialCount==0)  {  materialCount=1;  }
    printf("LoadObj: material count %i\n", materialCount);
    
    char matNames[materialCount][1024];
    char txNames[materialCount][1024];
    Colour colours[materialCount];
    // set the first colour to white in case we have no
    // materials...
    colours[0] = WHITE;

    // clear out the texture names strings
    for (int i=0; i<materialCount; i++) {
        txNames[i][0]='\0';
    }

    // get the textures and colours from the materials file
    line = mtldata;
    
    if (line) {  // is there a mtl file?
        int m=0;
        while(line[0]!='\0') 
        {
            // save each of the material names
            if (strncmp("newmtl ", line, 7)==0) {
                strCpyEOL(line+7, matNames[m]);
                m++;
            }
            
            // TODO implement the other map types here
            
            // diffuse colour
            if (strncmp("Kd ", line, 3)==0) {
                float r,g,b;
                sscanf( line+3, "%f %f %f", &r, &g, &b );
                colours[m-1].r = r;// * 255;
                colours[m-1].g = g;// * 255;
                colours[m-1].b = b;// * 255;
                colours[m-1].a = 1;//255;
            }
            
            // diffuse map
            if (strncmp("map_Kd", line, 6)==0) {
                strCpyEOL(line+7, txNames[m-1]);
            }
      
            line = nextLine(line);
        }
    }
    
    line = objdata;
    int currentMat = 0;
    
    // temporary storage for the vertex information
    float verts[vc*3];
    float tx[tc*2];
    float norms[nc*3];
    
    // indexes
    int vi=0, ti=0, ni=0;

    // count up the verts for each material
    // get all the vert data etc ready to
    // split between materials
    while (line[0] != '\0') 
    {
        if (strncmp("usemtl ", line, 7)==0) {
            strCpyEOL(line+7, tmpStr);
            for (int i = 0; i < materialCount; i++) {
                // simple linear search (KISS principle)
                // usually only a handful of materials so not
                // worth the overhead of hashmap or other techniques
                if (strcmp(tmpStr, matNames[i])==0) {
                    currentMat = i;
                    break;
                }
            } 
        }
        
        // count and collect the faces
        // at this point some faces might have more that 3 verts
        if (strncmp("f ", line, 2)==0) {
            strCpyEOL(line+2, tmpStr);
            faceInfo *f = parseFace(tmpStr);
            addFace(f, currentMat);
        }
        
        // store all the vertices positions
        if (strncmp("v ", line, 2)==0) {
            float v0,v1,v2;
            sscanf( line+2, "%f %f %f", &v0, &v1, &v2 );
            
            verts[vi] = v0;
            verts[vi+1] = v1;
            verts[vi+2] = v2;
            vi+=3;
        }
        
        // texture coords
        if (strncmp("vt ", line, 3)==0) {
            float t0,t1;
            sscanf( line+3, "%f %f", &t0, &t1 );
            
            tx[ti] = t0;
            tx[ti+1] = t1;
            ti+=2;
        }
        
        // normals
        if (strncmp("vn ", line, 3)==0) {
            float v0,v1,v2;
            sscanf( line+3, "%f %f %f", &v0, &v1, &v2 );
            
            norms[ni] = v0;
            norms[ni+1] = v1;
            norms[ni+2] = v2;
            ni+=3;
        }   
             
        line = nextLine(line);
    }


    //if (triangulate) {
        int ntf = 0;
        // triangulation
        for (int i=0; i < materialCount; i++) {
            // get all faces for each material in turn
            faceInfo* fi = allFaces[i].first;
            while(fi) {
                faceInfo* nextNode = fi->next;
                // if its not a triangle
                // turn the poly into a triangle fan
                if (fi->numVerts > 3) {
                    // loop through extra points making new (tri) faces
                    // set this faces facecount to 3
                    for (int j = 2; j < fi->numVerts-1; j++) {

                        faceInfo* nf = calloc(1, sizeof(faceInfo));
                        nf->vi = calloc(3, sizeof(int));
                        nf->ti = calloc(3, sizeof(int));
                        nf->ni = calloc(3, sizeof(int));
                        
                        nf->vi[0] = fi->vi[0];  nf->ti[0] = fi->ti[0];  nf->ni[0] = fi->ni[0];
                        nf->vi[1] = fi->vi[j];  nf->ti[1] = fi->ti[j];  nf->ni[1] = fi->ni[j];
                        nf->vi[2] = fi->vi[j+1];  nf->ti[2] = fi->ti[j+1];  nf->ni[2] = fi->ni[j+1];

                        nf->numVerts = 3;
                        addFace(nf, i);
                        ntf++;
                    }
                    fi->numVerts = 3;
                }
                
                fi = nextNode;
            }        
        }

        printf("LoadObj: model triangulation added %i tri's\n",ntf);
    //}
    
    
    model.meshCount = materialCount;
    model.materialCount = materialCount;
    
    // running index counts for each material indexes
    int mvi[materialCount];
    int mti[materialCount];
    int mni[materialCount];
    
    model.meshMaterial = calloc(materialCount, sizeof(int));
    model.materials = (Material *)calloc(model.materialCount, sizeof(Material));
    model.meshes = calloc(materialCount, sizeof(Mesh));

    
    // finally all the meta is gathered, build the meshes...
    
    int tfc = 0;
    // first set up the model materials
    for (int i=0; i < materialCount; i++) {
        printf("LoadObj: material %i %s face count %i\n",i,matNames[i],allFaces[i].faceCount);
        tfc += allFaces[i].faceCount;
        mvi[i]=0; mti[i]=0; mni[i]=0;

        model.meshes[i].vertexCount = allFaces[i].faceCount*3;
        
        // 3 pos verts = 9, 3 norm verts = 9, 2 UV's = 6, floats per "vertex" 
        model.meshes[i].buffer = calloc(allFaces[i].faceCount*(9+9+6)*3, sizeof(float));

        model.meshMaterial[i] = i;
        
        // TODO more than just DIFFUSE maps....!
        if (txNames[i][0]!='\0') {
            model.materials[i].difftx = loadTextureForBgfx(txNames[i]);
        } else {
            model.materials[i].difftx = defaultTx;
        }
        model.materials[i].diffuse = colours[i];
        
    }
    printf("LoadObj: total face count %i\n",tfc);

    // look up the vert data for the mesh buffers from the face indexes
    for (int i=0; i < materialCount; i++) {
        mvi[i] = 0; mti[i] = 0; mni[i] = 0;
    }        
    
    for (int i=0; i < materialCount; i++) {
        faceInfo* fi = allFaces[i].first;
        unsigned int bufpnt = 0;
        while(fi) {
            faceInfo* nextNode = fi->next;
            for (int p=0; p<3; p++) {                
                int idx;
                // never seen it in the wild but according to spec
                // -tive indexes are relative to the end of the buffer
                if (fi->vi[p] < 0) {  fi->vi[p] = vc - fi->vi[p];  }
                idx = (fi->vi[p]-1)*3;
                model.meshes[i].buffer[bufpnt] = verts[idx];
                model.meshes[i].buffer[bufpnt+1] = verts[idx+1];
                model.meshes[i].buffer[bufpnt+2] = verts[idx+2];
                mvi[i]+=3;

                if (fi->ni[p] < 0) {  fi->ni[p] = nc - fi->ni[p];  }
                if (fi->ni[p]) {
                    idx = (fi->ni[p]-1)*3;
                    model.meshes[i].buffer[bufpnt+3] = norms[idx];
                    model.meshes[i].buffer[bufpnt+4] = norms[idx+1];
                    model.meshes[i].buffer[bufpnt+5] = norms[idx+2];
                    mni[i]+=3;
                } else {
                    mni[i]+=3;
                }
                
                if (fi->ti[p] < 0) {  fi->ti[p] = tc - fi->ti[p];  }
                if (fi->ti[p] != 0) {
                    idx = (fi->ti[p]-1)*2;
                    model.meshes[i].buffer[bufpnt+6] = tx[idx];
                    model.meshes[i].buffer[bufpnt+7] = tx[idx+1];
                    mti[i]+=2;
                } else {
                    mti[i]+=2;
                }
                bufpnt +=8;
            }
            fi = nextNode;
        }
    }
    
    // upload all the meshes to the GPU
    model.transform = m4_identity();
    for (int i = 0; i < materialCount; i++) {
        const bgfx_memory_t* vbmem = bgfx_copy(model.meshes[i].buffer , sizeof(PosNormUvVertex) * model.meshes[i].vertexCount);
        model.meshes[i].vbh = bgfx_create_vertex_buffer(vbmem, &objvLayout, BGFX_BUFFER_NONE); 
    }

    // restore current directory (changed for mtl and texture loading)
    CHDIR(currentDir);
    
    // get rid of the linked list of face indexes
    releaseFaces();
    
    free(objdata);
    free(mtldata);

    return model;
}
