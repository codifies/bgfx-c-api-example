
typedef struct Colour {
    float r;
    float g;
    float b;
    float a;
} Colour;


typedef struct PosNormUvVertex
{
    vec3_t pos;
    vec3_t norm;
    float u,v;
} PosNormUvVertex;




typedef struct Mesh {
    int vertexCount;
    float *buffer;
    bgfx_vertex_buffer_handle_t vbh;
} Mesh;

typedef struct Material 
{
    bgfx_program_handle_t shader;
    
    Colour diffuse;
    bgfx_texture_handle_t difftx;
    
    bgfx_uniform_handle_t diffcU;
    bgfx_uniform_handle_t difftxU;
    
    // TODO other properties here like specular etc
} Material;

typedef struct Model {
    int meshCount;
    int materialCount;
    
    int *meshMaterial;
    Material *materials;
    Mesh *meshes;
    
    mat4_t transform;
} Model;


#define WHITE (Colour){ 1, 1, 1, 1 };


Model LoadObj(const char* filename);
bgfx_texture_handle_t loadTextureForBgfx( const char* fileName);
bgfx_shader_handle_t loadShader(const char *FILENAME);
void initModels();
void shutdownModels();
void submitMesh( Mesh* m, Material mat);
void submitModel( Model* m );
void freeMaterial(Material m);
void freeMesh(Mesh* m);
void freeModel(Model* m);
void applyObjShader(Model* mod, 
                        bgfx_program_handle_t program, 
                        bgfx_uniform_handle_t diffcU, 
                        bgfx_uniform_handle_t difftxU);
