

#define MAX_FILEPATH_LENGTH     4096

#if defined(_WIN32)
    #include <direct.h>             // Required for: _getch(), _chdir()
    #define GETCWD _getcwd          // NOTE: MSDN recommends not to use getcwd(), chdir()
    #define CHDIR _chdir
    #include <io.h>                 // Required for _access() [Used in FileExists()]
#else
    #include <unistd.h>             // Required for: getch(), chdir() (POSIX), access()
    #define GETCWD getcwd
    #define CHDIR chdir
#endif


typedef struct ImgData 
{
    int width;
    int height;
    int bpp;
    unsigned char* data;
} ImgData;



const char *GetDirectoryPath(const char *filePath);
const char *GetWorkingDirectory(void);
char *LoadFileText(const char *fileName);
ImgData loadImage(const char* fileName);
void freeImageData(ImgData img);
