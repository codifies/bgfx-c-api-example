## BGFX C API example ##

This is an example of using BGFX via with its C API

It has its own wavefront OBJ loader, and a very simple lighting shader 
for multiple point lights, each OBJ is separated into its different 
materials and the material diffuse colour is taken into account
as well as the optional map (texture)

### building ###

git submodule init
git submodule update --depth=1

cd bgfx

../bx/tools/bin/linux/genie --with-tools --gcc=linux-gcc --platform=x64 --os=linux --cc=gcc  gmake

make linux-debug64 -j$(nproc)

once bgfx has been made, you can then compile the example
by running make in the obj-load directory

some minimal work will need to be done to get this working on windows
there's plenty of TODO's so contributions are most welcome and appreciated





